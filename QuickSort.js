(function (root, QuickSort) {
    if (typeof define === 'function' && define.amd)
        define([], new QuickSort())
    else if (typeof exports === 'object')
        module.exports = new QuickSort()
    else root.QuickSort = new QuickSort()
})(this, class QuickSort {
    quickSort(arr, start, end) {
        if (start >= end) return

        const index = this.partition(arr, start, end)

        this.quickSort(arr, start, index - 1)
        this.quickSort(arr, index + 1, end)
    }

    partition(arr, start, end) {
        let pivotIndex = start
        const pivotValue = arr[end]

        for (let i = start; i < end; i++)
            if (arr[i] < pivotValue) this.swap(arr, i, pivotIndex++)

        this.swap(arr, pivotIndex, end)
        return pivotIndex
    }

    swap(arr, a, b) {
        const temp = arr[a]
        arr[a] = arr[b]
        arr[b] = temp
    }
})