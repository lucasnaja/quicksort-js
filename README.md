# QuickSort.js

### Made for learn. (default Chrome JS Array.prototype.sort() function is a QuickSort Method)

### How to use

- Web (Javascript)
```html
    <script src="src/QuickSort.min.js"></script>
    <script>
        function generateArray(length) {
            const array = []
            for (let i = 0; i < length; i++)
                array.push(parseInt(Math.random() * 100) + 1, 10)

            return array
        }

        const array = generateArray(10)
        console.log(array)
        QuickSort.quickSort(array, 0, array.length - 1)
        console.log(array)
    </script>
```

- Server (Node.js)
```js
    const QuickSort = require('./QuickSort')

    function generateArray(length) {
        const array = []
        for (let i = 0; i < length; i++)
            array.push(parseInt(Math.random() * 100) + 1, 10)

        return array
    }

    const array = generateArray(10)
    console.log(array)
    QuickSort.quickSort(array, 0, array.length - 1)
    console.log(array)
```