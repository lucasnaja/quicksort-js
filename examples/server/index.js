
const QuickSort = require('./QuickSort')

function generateArray(length) {
    const array = []
    for (let i = 0; i < length; i++)
        array.push(parseInt(Math.random() * 100) + 1, 10)

    return array
}

const array = generateArray(10)
console.log(array)
QuickSort.quickSort(array, 0, array.length - 1)
console.log(array)